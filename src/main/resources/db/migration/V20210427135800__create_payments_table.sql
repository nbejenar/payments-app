create table payments (
    id varchar(36) not null,
    currency varchar(3) not null,
    amount decimal(19,2) not null,
    short_reference varchar(255) not null,
    beneficiary_first_name varchar(255) not null,
    beneficiary_last_name varchar(255) not null,
    beneficiary_bank_account_number varchar(31) not null,
    payer_bank_account_number varchar(31) not null,
    status varchar(9) not null,
    primary key (id)
)