package com.currencycloud.shadowing.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "payments")
public class Payment {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private String id;

    @Pattern(regexp = "MDL", message = "only MDL currency supported")
    private String currency;

    @Positive(message = "should be grater than 0")
    private BigDecimal amount;

    @Pattern(regexp = "\\d{6}-[a-zA-Z0-9]{6}\\d{3}", message = "pattern: \\d{6}-[a-zA-Z0-9]{6}\\d{3}")
    private String shortReference;

    @NotBlank(message = "should not be empty")
    private String beneficiaryFirstName;

    @NotBlank(message = "should not be empty")
    private String beneficiaryLastName;

    @Pattern(regexp = "\\d{10,12}", message = "from 10 to 12 digits")
    private String beneficiaryBankAccountNumber;

    @Pattern(regexp = "\\d{10,12}", message = "from 10 to 12 digits")
    private String payerBankAccountNumber;

    @Enumerated(EnumType.STRING)
    private Status status;
}
