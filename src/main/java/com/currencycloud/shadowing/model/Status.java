package com.currencycloud.shadowing.model;

public enum Status {
    NEW, VALIDATED, SUBMITTED, FAILED
}
