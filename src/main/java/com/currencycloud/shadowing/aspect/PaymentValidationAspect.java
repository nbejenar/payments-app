package com.currencycloud.shadowing.aspect;

import com.currencycloud.shadowing.model.Payment;
import com.currencycloud.shadowing.model.Status;
import com.currencycloud.shadowing.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class PaymentValidationAspect {

    public final PaymentRepository paymentRepository;
    public Validator validator;

    @PostConstruct
    private void init() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Around("execution(* com.currencycloud.shadowing.amqp.facade.PaymentFacade.save(..))))")
    public Object validatePayment(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Payment payment = null;
        try {
            payment = (Payment) proceedingJoinPoint.proceed();
            return payment;
        } finally {
            if (payment != null) {
                log.info("Validating {}", payment);
                Set<ConstraintViolation<Payment>> violationSet = validator.validate(payment);
                if (!violationSet.isEmpty()) {
                    log.info("Validation failed");
                    violationSet.forEach(v -> log.info("{}: {}", v.getPropertyPath(), v.getMessage()));
                    payment.setStatus(Status.FAILED);
                } else {
                    log.info("Validation successful");
                    payment.setStatus(Status.VALIDATED);
                }
                paymentRepository.save(payment);
            }
        }
    }
}