package com.currencycloud.shadowing.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    @Before("@annotation(org.springframework.amqp.rabbit.annotation.RabbitListener)")
    public void logConsumer(JoinPoint joinPoint) {
        log.info("Executing listener {} with args {}", joinPoint.getSignature(), joinPoint.getArgs());
    }
}
