package com.currencycloud.shadowing.repository;

import com.currencycloud.shadowing.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, String> {

    @Override
    <S extends Payment> S save(S s);
}
