package com.currencycloud.shadowing.amqp.handlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.RabbitListenerErrorHandler;
import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
class RabbitRetryHandler implements RabbitListenerErrorHandler {

    @Override
    public Object handleError(Message amqpMessage, org.springframework.messaging.Message<?> message,
                              ListenerExecutionFailedException exception) throws Exception {
        log.info("Redelivered: {}", amqpMessage.getMessageProperties().isRedelivered());
        if (amqpMessage.getMessageProperties().isRedelivered()) {
            throw new AmqpRejectAndDontRequeueException(exception.getMessage());
        } else {
            throw exception;
        }
    }

}