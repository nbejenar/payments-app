package com.currencycloud.shadowing.amqp.facade;

import com.currencycloud.shadowing.amqp.dto.PaymentDTO;
import com.currencycloud.shadowing.amqp.dto.PaymentMapper;
import com.currencycloud.shadowing.model.Payment;
import com.currencycloud.shadowing.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PaymentFacade {

    private final PaymentRepository paymentRepository;

    public Payment save(PaymentDTO paymentDTO){
        return paymentRepository.save(PaymentMapper.DtoToEntity(paymentDTO));
    }
}
