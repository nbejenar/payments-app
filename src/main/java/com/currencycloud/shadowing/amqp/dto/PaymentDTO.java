package com.currencycloud.shadowing.amqp.dto;

import com.currencycloud.shadowing.model.Payment;
import com.currencycloud.shadowing.model.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDTO {

    private String currency;
    private BigDecimal amount;
    private String shortReference;
    private String beneficiaryFirstName;
    private String beneficiaryLastName;
    private String beneficiaryBankAccountNumber;
    private String payerBankAccountNumber;
}
