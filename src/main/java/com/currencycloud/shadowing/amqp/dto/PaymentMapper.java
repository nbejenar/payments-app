package com.currencycloud.shadowing.amqp.dto;

import com.currencycloud.shadowing.model.Payment;
import com.currencycloud.shadowing.model.Status;

public class PaymentMapper {

    public static Payment DtoToEntity(PaymentDTO paymentDTO) {
        return Payment.builder()
                .currency(paymentDTO.getCurrency())
                .amount(paymentDTO.getAmount())
                .shortReference(paymentDTO.getShortReference())
                .beneficiaryFirstName(paymentDTO.getBeneficiaryFirstName())
                .beneficiaryLastName(paymentDTO.getBeneficiaryLastName())
                .beneficiaryBankAccountNumber(paymentDTO.getBeneficiaryBankAccountNumber())
                .payerBankAccountNumber(paymentDTO.getPayerBankAccountNumber())
                .status(Status.NEW) // should it be defaulted elsewhere?
                .build();
    }
}
