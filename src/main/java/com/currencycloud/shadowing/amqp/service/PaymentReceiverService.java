package com.currencycloud.shadowing.amqp.service;

import com.currencycloud.shadowing.amqp.dto.PaymentDTO;
import com.currencycloud.shadowing.amqp.facade.PaymentFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Component
@Service
public class PaymentReceiverService {

    private final PaymentFacade paymentFacade;

    @RabbitListener(queues = {"${rabbitmq.queue.name}"}, errorHandler = "rabbitRetryHandler")
    @Transactional
    public void handle(PaymentDTO paymentDTO) {
        paymentFacade.save(paymentDTO);
    }
}
