
default payload
```json
{
"currency" : "MDL",
"amount" : 1000,
"shortReference": "123456aBcDeF123",
"beneficiaryFirstName": "John",
"beneficiaryLastName": "Doe",
"beneficiaryBankAccountNumber": "1234567890",
"payerBankAccountNumber": "12345678901"
}
```

- currency: MDL only
- amount: >0
- short-reference: regex [0-9]{6}-[a-zA-Z0-9]{6}[0-9]{3}
- beneficiary first name: non empty
- beneficiary last name: non empty
- beneficiary bank account number: regex 10-12 digits
- payer bank account number: regex 10-12 digits
